package com.sourceit.lesson4task2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Generator {
    public static List<Contacts> generate() {
        ArrayList<Contacts> list = new ArrayList<>();
        list.add(new Contacts("contact 1", "contact_1@icloud.com", "+380660000001", "г. Харьков ул. Гвардейцев-Широнинцев,1", R.drawable.photo1));
        list.add(new Contacts("contact 2", "contact_2@icloud.com", "+380660000002", "г. Харьков ул. Гвардейцев-Широнинцев,2", R.drawable.photo2));
        list.add(new Contacts("contact 3", "contact_3@icloud.com", "+380660000003", "г. Харьков ул. Гвардейцев-Широнинцев,3", R.drawable.photo3));
        list.add(new Contacts("contact 4", "contact_4@icloud.com", "+380660000004", "г. Харьков ул. Гвардейцев-Широнинцев,4", R.drawable.photo4));
        list.add(new Contacts("contact 5", "contact_5@icloud.com", "+380660000005", "г. Харьков ул. Гвардейцев-Широнинцев,5", R.drawable.photo5));
        list.add(new Contacts("contact 6", "contact_6@icloud.com", "+380660000006", "г. Харьков ул. Гвардейцев-Широнинцев,6", R.drawable.photo6));
        list.add(new Contacts("contact 7", "contact_7@icloud.com", "+380660000007", "г. Харьков ул. Гвардейцев-Широнинцев,7", R.drawable.photo1));
        list.add(new Contacts("contact 8", "contact_8@icloud.com", "+380660000008", "г. Харьков ул. Гвардейцев-Широнинцев,8", R.drawable.photo2));
        list.add(new Contacts("contact 9", "contact_9@icloud.com", "+380660000009", "г. Харьков ул. Гвардейцев-Широнинцев,9", R.drawable.photo3));
        list.add(new Contacts("contact 10", "contact_10@icloud.com", "+380660000010", "г. Харьков ул. Гвардейцев-Широнинцев,10", R.drawable.photo4));
        list.add(new Contacts("contact 11", "contact_11@icloud.com", "+380660000011", "г. Харьков ул. Гвардейцев-Широнинцев,11", R.drawable.photo5));
        list.add(new Contacts("contact 12", "contact_12@icloud.com", "+380660000012", "г. Харьков ул. Гвардейцев-Широнинцев,12", R.drawable.photo6));

        return list;
    }
}
