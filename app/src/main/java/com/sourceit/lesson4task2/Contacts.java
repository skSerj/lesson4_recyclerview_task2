package com.sourceit.lesson4task2;

public class Contacts {
    private String name;
    private String mail;
    private String phone_number;
    private String address;
    private int photo;

    public Contacts(String name, String mail, String phone_number, String address, int photo) {
        this.name = name;
        this.mail = mail;
        this.phone_number = phone_number;
        this.address = address;
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getAddress() {
        return address;
    }

    public int getPhoto() {
        return photo;
    }
}

