package com.sourceit.lesson4task2;

public interface ItemOnClickListener {
    void onItemClick(Contacts contacts);
}
