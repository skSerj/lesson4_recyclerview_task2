package com.sourceit.lesson4task2;

import android.content.Context;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactsViewHolder> {

    private Context context;
    private List<Contacts> contactsList;
    private ItemOnClickListener listener;

    public ContactsAdapter(Context context, List<Contacts> contactsList, ItemOnClickListener listener) {
        this.context = context;
        this.contactsList = contactsList;
        this.listener = listener;
    }

    public void addContacts(Contacts contacts) {
        contactsList.add(contacts);
        notifyItemInserted(contactsList.size() - 1);
    }

    @NonNull
    @Override
    public ContactsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_contacts, parent, false);
        return new ContactsViewHolder(v, listener);
    }

    @Override
    public int getItemCount() {
        return contactsList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ContactsViewHolder holder, int position) {
        holder.bind(contactsList.get(position));
    }

    static class ContactsViewHolder extends RecyclerView.ViewHolder {
        ItemOnClickListener listener;
        TextView name;
        TextView mail;
        ImageView photo;
        View root;

        public ContactsViewHolder(@NonNull View itemView, final ItemOnClickListener listener) {
            super(itemView);
            this.listener = listener;
            root = itemView.findViewById(R.id.root);
            name = itemView.findViewById(R.id.contact_name);
            mail = itemView.findViewById(R.id.contact_mail);
            photo = itemView.findViewById(R.id.contact_icon);

        }

        public void bind(final Contacts contacts) {
            name.setText(contacts.getName());
            mail.setText(contacts.getMail());
            photo.setImageResource(contacts.getPhoto());
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(contacts);
                }
            });
        }
    }
}
