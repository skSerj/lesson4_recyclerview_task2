package com.sourceit.lesson4task2;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class ListFragment extends Fragment implements ItemOnClickListener {
    private RecyclerView recyclerView;
    private Button addContact;
    private ContactsAdapter contactsAdapter;

    public static ListFragment newInstance() {
        return new ListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle b) {
        View root = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = root.findViewById(R.id.contact_list);
        addContact = root.findViewById(R.id.action);
        addContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactsAdapter.addContacts(new Contacts("new User", "new user@mail.ru", "+380501236598", "г.Покровск", R.drawable.photo2));
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(root.getContext()));
        contactsAdapter = new ContactsAdapter(root.getContext(), Generator.generate(), this);
        recyclerView.setAdapter(contactsAdapter);
        return root;
    }

    @SuppressLint("ResourceType")
    @Override
    public void onItemClick(Contacts contacts) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.main_container, new ContactsFragment(contacts))
                .addToBackStack(null)
                .commit();
    }
}
