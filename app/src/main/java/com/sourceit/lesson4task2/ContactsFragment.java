package com.sourceit.lesson4task2;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class ContactsFragment extends Fragment {
    private String name;
    private String mail;
    private String address;
    private String phoneNumber;
    private int photo;

    private TextView viewName;
    private TextView viewMail;
    private TextView viewAddress;
    private TextView viewPhoneNumber;
    private ImageView viewPhoto;

    public ContactsFragment(Contacts contacts) {
        name = contacts.getName();
        mail = contacts.getMail();
        address = contacts.getAddress();
        phoneNumber = contacts.getPhone_number();
        photo = contacts.getPhoto();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle b) {
        View v = inflater.inflate(R.layout.fragment_contacts, container, false);
        viewName = v.findViewById(R.id.contact_name);
        viewMail = v.findViewById(R.id.contact_mail);
        viewAddress = v.findViewById(R.id.contact_address);
        viewPhoneNumber = v.findViewById(R.id.contact_phone_number);
        viewPhoto = v.findViewById(R.id.contact_icon);

        viewName.setText(name);
        viewAddress.setText(address);
        viewMail.setText(mail);
        viewPhoneNumber.setText(phoneNumber);
        viewPhoto.setImageResource(photo);

        return v;
    }
}
